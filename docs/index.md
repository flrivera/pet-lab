<img src="Title_1.png" alt="hi" class="inline"/>
## Approximately 67% of households have pets in the United States, yet toxicity levels of common flowers are not readily available. 

Pet-Lab will:
- Identify the flower type depending on unique charasteristics.
- Search database to provide user with toxicity information (severity, common symptoms) as well as flower description to aide in detection confirmation.


## Existing products and gap in the Market

| <img src="apcc.PNG" alt="hi" class="inline"/>        |
| -------------- |
| APCC by the ASPCA does have information about the toxicity of plants and flowers but lacks identification capabilities.   |


## How to tell plants and flowers apart?



### Average images and PCA of the 5 most common classes (rose, dandelion, daisy, tulip, sunflower): 
#### This showcases how finding properties(features) of each image can help differentiate between the 5 classes.
      
 <p float="left">
  <img src="Averages.png" width="33%"  height="250"/>
  <img src="dandelion_Eigenstate-1.png" width="33%" height="250"/>
</p>


### Fitting and making predictions: 

#### Preprocessing:

- using the rembg package https://pypi.org/project/rembg/ I remove the background from the training and validation images,this allows the algoritm to just focus on the details of the image we want to classify.
- In order to minimize the effects of camera quality and saturation settings on Images from different sources we re-scale both training and validation datasets.  We also apply zooming and shearing to just the training dataset to lower risk of overfitting before fitting the model to the dataset.

#### I use transfer learning as a way of generating features that differentiate our 5 unique classes

- Using an already trained convolutional neural network (Inception) to generate class features optimizing the fit to our dataset and testing our predictions:
https://towardsdatascience.com/review-inception-v4-evolved-from-googlenet-merged-with-resnet-idea-image-classification-5e8c339d18bc


# Current and Future Developments
<img src="Presentation 3.jpg" />


